class AutonomousCar(): CarSensors {

    private var positionX = 0
    private var positionY = 0

    override fun isThereSomethingAt(direction: Direction): Boolean {
        println("Mirando hacia $direction")
        println("Hay obstáculo?")
        var isSomething = false
        when (direction){
            Direction.FRONT -> if (isSomething == false){
                go(Direction.FRONT )
            } else {
                isSomething = true
                isThereSomethingAt(Direction.RIGHT)
            }
            Direction.RIGHT -> if (isSomething == false){
                go(Direction.RIGHT)
            } else {
                isSomething = true
                isThereSomethingAt(Direction.LEFT)
            }
            Direction.LEFT -> if (isSomething == false){
                go(Direction.LEFT)
            } else {
               isSomething = true
                stop()
            }
        }
        return isSomething
    }

    override fun go(direction: Direction) {
        when (direction){
            Direction.FRONT -> positionY += 1
            Direction.RIGHT -> positionX += 1
            Direction.LEFT -> positionX -= 1
        }
        println("El coche se mueve hacia $direction")
    }

    override fun stop() {
        println("Coche parado\n")
    }
    fun doNextStep(n: Int){
        repeat(n){
            isThereSomethingAt(Direction.FRONT)
            println("Posición Actual: (${positionY},${positionX})")
        }

    }

}