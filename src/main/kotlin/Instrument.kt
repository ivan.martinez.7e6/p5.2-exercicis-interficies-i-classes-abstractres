open class Instrument {
    open fun makeSounds(times: Int):String{
        return  ""
    }
}

class Triangle(
    private val resonancia: Int
): Instrument() {
    override fun makeSounds(times: Int): String {
        var sound = ""
        repeat(times){
            sound += "T"
            for (i in 1..resonancia){
                sound += "I"
            }
            sound += "NC "
        }
        return sound
    }
}

class Drump(
    val to: String
): Instrument(){
    override fun makeSounds(times: Int): String {
        var sound = ""
        var toSound = ""
        repeat(3){
            toSound += to
        }
        repeat(times){
            sound += "T${toSound}M "
        }
        return sound
    }

}