import kotlinx.serialization.*
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.Json.Default.decodeFromString
import java.io.File

@Polymorphic
@Serializable
sealed class Question{
    abstract val enunciat: String
    abstract val resposta: String
    abstract fun answerQuestion(userAnswer: String): Boolean
    abstract fun showQuestion(): String
}

@SerialName("FreeTextQuestion")
@Serializable
class FreeTextQuestion(
    override val enunciat: String,
    override val  resposta: String): Question(){
    override fun answerQuestion(userAnswer: String): Boolean {
        return userAnswer.uppercase() == resposta.uppercase()
    }

    override fun showQuestion(): String {
       return enunciat
    }

}

@SerialName("MultipleChoiceQuestion")
@Serializable
class MultipleChoiceQuestion(
    override val enunciat: String,
    override val  resposta: String,
    val choice1: String,
    val choice2: String,
    val choice3: String): Question(){
    override fun answerQuestion(userAnswer: String): Boolean {
        return userAnswer.uppercase() == resposta.uppercase()
    }
    override fun showQuestion(): String {
        return "$enunciat\nOpcions\n$choice1 | $choice2 | $choice3"
    }

}
//Genera lista de preguntas desde el json
class Quiz{
    private val file = File("src/main/kotlin/archivos/preguntas.json")

    fun makeQuiz(): MutableList<Question> {
        val listaPreguntas = mutableListOf<Question>()
        for (i in file.readLines()){
            val question = Json.decodeFromString<Question>(i)
            listaPreguntas.add(question)
        }
        return listaPreguntas
    }

}