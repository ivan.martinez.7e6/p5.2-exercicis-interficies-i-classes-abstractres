import java.util.*

class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`).useLocale(Locale.UK)
) : GymControlReader {

    override fun nextId() = scanner.next()

    private val log = mutableSetOf<String>()

    fun tornos(id: String): String{
        return if (id !in log){
            log.add(id)
            "$id entrada"
        } else{
            log.remove(id)
            "$id sortida"
        }
    }
}
