import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import kotlinx.serialization.modules.SerializersModule
import java.io.File

fun main(){
    //makeJson()

    val quiz = Quiz().makeQuiz()
    var correctAnswers = 0
    for (question in quiz){
        println(question.showQuestion())
        val userAnswer = scanner.nextLine().uppercase()
        if (question.answerQuestion(userAnswer)){
           correctAnswers++
           println("Correcte")
       } else println("Incorrecte")
    }
    println("Has encertat $correctAnswers preguntes de ${quiz.size}")

}

fun makeJson(){
    val listOfQuestions = listOf<Question>(
        FreeTextQuestion("Quin element conforma la capa d'ozò?", "Oxigen"),
        FreeTextQuestion("Quin és l'animal terrestre que posa l'ou més gran?", "Estruç"),
        MultipleChoiceQuestion("Quin és el nom real de Lord Voldemort?", "Tom Riddle",
            "Draco Malfoy", "Snape", "Tom Riddle"),
        FreeTextQuestion("Quin és el punt d'ebullició de l'aigua? (En Cº)", "100"),
        MultipleChoiceQuestion("La llum és una partícula o una ona?",
            "Les dues coses", "Ona", "Particula", "Les dues coses"),
        MultipleChoiceQuestion("Quina és la galàxia espiral més pròxima a la Via Làctia?", "Andrómeda", "Centaure A", "Bode", "Andrómeda"),
        FreeTextQuestion("Quants oceans hi han al planeta?", "5"),
        MultipleChoiceQuestion("En la Bella i la Bèstia, qui és el vilà?","Gastón", "La Bèstia","Maurici", "Lloansi")

    )


    for (i in listOfQuestions){
        val questionToJson = Json.encodeToString(i)

        File("src/main/kotlin/archivos/preguntas.json").appendText(questionToJson+"\n")

    }

}