

fun main() {
    val students = mutableListOf("Carlos","Marta","Pol","Raul")
    repeat(2){
        val randomGrade = TEXTGRADE.values().random()
        val randomStudent = students.random()
        val student1 = Student(randomStudent, randomGrade)
        students.remove(randomStudent)
        println(student1.toString())
    }

}