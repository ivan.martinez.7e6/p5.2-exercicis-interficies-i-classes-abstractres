class Student(
    private val nom: String,
    private val textGrade: TEXTGRADE
){
    override fun toString(): String{
        return "Student(nom='$nom',textGrade=$textGrade)"
    }
}